﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeepInFrame : MonoBehaviour
{
    public Vector2 MinAllowedRegion = new Vector2(0, 0);
    public Vector2 MaxAllowedRegion = new Vector2(1, 1);

    private Rigidbody rb;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void LateUpdate ()
    {
        Vector3 pos = Camera.main.WorldToViewportPoint(transform.position);
        pos.x = Mathf.Clamp(pos.x, MinAllowedRegion.x, MaxAllowedRegion.x);
        pos.y = Mathf.Clamp(pos.y, MinAllowedRegion.y, MaxAllowedRegion.y);
        //Debug.Log(pos);
        transform.position = Camera.main.ViewportToWorldPoint(pos);
        //rb.MovePosition(Camera.main.ViewportToWorldPoint(pos));
    }
}
