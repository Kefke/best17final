﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
    public float movement_speed = .1f;
    public float rotation_speed = 2f;
    public int number_of_tries_before_rotation = 60;
    public Vector2 rotation_duration;
    public float lifeTime = 10;

    private const string ANIMATION_DEATH = "Die";

    private Rigidbody rb;
    private Animator anim = null;
    private bool isHit = false;
    private float randomSpeed = 0;
    private int rotationsLeft = 0;

    // Use this for initialization
    void Start ()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    private void Update()
    {
        lifeTime -= Time.deltaTime;
        if (lifeTime <= 0)
            Destroy(gameObject);
    }
    void FixedUpdate()
    {
        // Calculate movement
        Vector3 translation = transform.forward * movement_speed;
        rb.MovePosition(transform.position + translation);

        if (rotationsLeft > 0)
        {
            transform.Rotate(Vector3.up, randomSpeed * rotation_speed);
            --rotationsLeft;
        }
        else
        {
            // Random rotation
            if (number_of_tries_before_rotation > -1)
            {
                if (Random.Range(0, number_of_tries_before_rotation) == 0)
                {
                    randomSpeed = Random.Range(-1.0f, 1.0f);
                    rotationsLeft = (int)Random.Range(rotation_duration.x, rotation_duration.y);
                    transform.Rotate(Vector3.up, randomSpeed * rotation_speed);
                }
            }
        }
    }

    private void Hit()
    {
        if (isHit) return;
        isHit = true;
        movement_speed = 0;
        GameMainScript.EnemyKilled();
        if (anim)
        {
            anim.Play(ANIMATION_DEATH);
        }
        else
            Die();
    }

    private void Die()
    {
        Destroy(gameObject);
    }
}
