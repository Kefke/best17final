﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseGame : MonoBehaviour
{
    public string GAMESCENE = "TestGame";
    public GameObject pauseScreen = null;

    public void Update()
    {
        if (Input.GetKey("p"))
        {
            Time.timeScale = 0;
            pauseScreen.SetActive(true);
            Debug.Log("Pressed P");
        }
    }

    public void ResetGame()
    {
        SceneManager.LoadScene(GAMESCENE);
        Time.timeScale = 1;
    }

    public void ContinueGame()
    {
        Time.timeScale = 1;
        pauseScreen.SetActive(false);
    }

    public void ExitGame()
    {
        Debug.Log("Quit Button");
        Application.Quit();
    }
}