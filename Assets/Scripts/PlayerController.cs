﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private const string ANIMATOR_SPEED = "Speed";
    private const string ANIMATOR_SHOOT_PRIMARY = "Shoot_Primary";
    private const string ANIMATOR_SHOOT_SECONDARY = "Shoot_Secondary";
    private const string ANIMATION_DEATH = "Die_Player";

    public float movement_speed = 0.5f;
    public float rotation_speed = 2f;
    public GameObject deathScreen = null;
    private Animator anim;
    private Rigidbody rb;
    private Vector3 lastPos;
    public GameObject pauseScreen = null;
    private bool isAlive = true;

    // Use this for initialization
    void Start ()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
    }

    // FixedUpdate called once per fixed time. check Edit->ProjectSettings->Time
    private void FixedUpdate()
    {
        if (!isAlive)
            return;
        // Getting controller input values
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        bool ShootPrimary = Input.GetButton("Fire1");
        bool ShootSecondary = Input.GetButton("Fire2");

        // Secondary fire has priority over primary
        if (ShootSecondary)
            ShootPrimary = false;

        // Set speed parameter
        anim.SetFloat(ANIMATOR_SPEED, v);

        // Set fire parameters
        anim.SetBool(ANIMATOR_SHOOT_PRIMARY, ShootPrimary);
        anim.SetBool(ANIMATOR_SHOOT_SECONDARY, ShootSecondary);

        // Calculate movement
        Vector3 translation = transform.forward * v * movement_speed;
        rb.MovePosition(transform.position + translation);
        // Calculate speed
        Vector3 speedVector = transform.position - lastPos;
        float speed = speedVector.magnitude;
        // Ship can only rotate when moving
        transform.Rotate(Vector3.up, speed * h * rotation_speed);

        // Save new position
        lastPos = transform.position;
    }

    // Update is called once per frame
    void Update ()
    {
        if (Input.GetKey("p"))
        {
            Time.timeScale = 0;
            pauseScreen.SetActive(true);
            Debug.Log("Pressed P");
        }
    }

    private void FirePrimary()
    {
        Debug.Log("Fire primary");
    }

    private void FireSecondary()
    {
        Debug.Log("Fire secondary");
    }

    private void Die()
    {
        Debug.Log("Die");
        anim.enabled = false;
        deathScreen.SetActive(true);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!isAlive)
            return;
        //Debug.Log("Touched " + collision.gameObject.tag);
        if (collision.gameObject.tag == "Enemy")
        {
            Debug.Log("You've died!");
            isAlive = false;
            anim.Play(ANIMATION_DEATH);
            //anim.SetBool(ANIMATOR_ALIVE, isAlive);
        }
    }
}
