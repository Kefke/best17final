using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GetKilled : MonoBehaviour {

    public string GAMESCENE = "TestGame";
    public GameObject pauseScreen = null;

    public void ResetGame()
    {
        SceneManager.LoadScene(GAMESCENE);
        Time.timeScale = 1;
    }


    public void ExitGame()
    {
        Application.Quit();
    }
}