using UnityEngine;


public class BulletScript : MonoBehaviour
{
  public float speed = 10f;
    public float lifeTime = 10;

    private Rigidbody rigidbody;

  private void OnCollisionEnter( Collision collision )
  {
    if ( collision.gameObject.tag == "NoHit" )
      return;
    collision.gameObject.SendMessage( "Hit", SendMessageOptions.DontRequireReceiver );
    Destroy( gameObject );
  }

  // Use this for initialization
  private void Start()
  {
    rigidbody = GetComponent<Rigidbody>();
    rigidbody.AddForce( transform.forward * speed, ForceMode.Impulse );
  }

    private void Update()
    {
        lifeTime -= Time.deltaTime;
        if (lifeTime <= 0)
            Destroy(gameObject);
    }
}
