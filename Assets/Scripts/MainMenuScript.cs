﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{
    public string GAMESCENE = "TestGame";
    public void StartGame()
    {
        Debug.Log("Started Game");
        SceneManager.LoadScene(GAMESCENE);
    }

    public void ExitGame()
    {
        Debug.Log("Quit Button");
        Application.Quit();
    }
}